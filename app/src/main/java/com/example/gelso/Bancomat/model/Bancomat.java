package com.example.gelso.Bancomat.model;


public class Bancomat {

	private int balance;

	//CONSTRUCTOR
	public Bancomat(int money) {
		this.balance = money;
	}
	
	public void PutMoney(int money) {
		this.balance = this.balance + money;	
	}
	
	public void Withdraw (int money) {
		this.balance = this.balance - money;
	}
	
	public int GetBalance() {
		return this.balance;
	}

}
